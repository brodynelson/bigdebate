import React, { Component } from 'react';
import {Grid, Row, Col, Image, Input, Button, ButtonGroup, Badge} from 'react-bootstrap';

export default class Card extends Component {
   
  
  constructor(props) {
    super(props);
    this.buttonPressed = this.buttonPressed.bind(this);
    this.state = {
    	voteCount: 0
    };
  }
   

  buttonPressed() {
  		console.log('votes:',this.state.voteCount);
  		var newCount = this.state.voteCount + 1;
   		this.setState({
   			voteCount: newCount
   		})
  }  


  render() {
  	
    return (
    			<div id='card'>
						<h1>Team {this.props.title}</h1>
		    			<Image src={this.props.imagePath} rounded/>
		    			<Button onClick={this.buttonPressed}>Vote</Button>
		    			<div>Votes:<Badge>{this.state.voteCount}</Badge></div>
		    	</div>

    )
  }
}