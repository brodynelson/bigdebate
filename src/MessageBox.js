import React, {Component} from 'react';

export default class MessageBox extends Component {

	render() {
		console.log('My props', this.props);
		return (
				<div id='message-box'> 
					<h1>{this.props.message}</h1>
				</div>
			)
		} 

}