import React, { Component } from 'react';
import { Button, Grid, Row, Col } from 'react-bootstrap';
import Card from './Card.js';
import HeadingBox from './HeadingBox.js';
import MessageBox  from './MessageBox.js';

export default class LandingPage extends Component {
	
	buttonPressed() {
		alert('Woot');
	}


	render() {

	    return (
	    	<Grid>
	    		<Row>
	    			<HeadingBox/>
	    		</Row>
	    		<Row>
	    			<Col md={3}>
	    				<Card title='Swift' imagePath='/resources/swift.jpg' />
	    			</Col>
		    		<Col md={3}>
		    			<Card title='Kanye' imagePath='/resources/kayne.jpg' />
		    		</Col>
		    		<Col md={3}>
		    			<Card title='Kim' imagePath='/resources/kim.jpg' />
		    		</Col>
	    		</Row>
	    		<Row>
	    			<MessageBox message={'Hello world'}/>
	    		</Row>
	    	</Grid>


	    );


	}
}