import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './App';
require('./style/style.less');

render(
  <AppContainer>
  	<App />
  </AppContainer>,
  document.getElementById('form_container')
);

if (module.hot) {
  module.hot.accept('./App', () => {

  	const app = require('./App').default;

    render(

      <AppContainer>
      	{app}
      </AppContainer>,
      document.getElementById('form_container')
    );
  });
}